#
# Copyright 2019 The Kathra Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Contributors:
#
#    IRT SystemX (https://www.kathra.org/)    
#
# 
# coding: utf-8

from setuptools import setup, find_packages

NAME = "kathra-core-model"
VERSION = "1.3.0"

# To install the library, run the following
#
# python setup.py install
# or
# pip install . (after cd to this folder)
# prerequisite: setuptools
# http://pypi.python.org/pypi/setuptools

setup(
    name=NAME,
    version=VERSION,
    description="Kathra Core Model",
    author_email="",
    url="",
    keywords=["Model","Core", "Kathra"],
    packages=find_packages(),
    include_package_data=True,
    long_description="""\
    Core modules for python models in kathra
    """
)

